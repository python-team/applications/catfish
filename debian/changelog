catfish (4.20.0-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 4.20.0
  * Build has switched from SetupTool + DistUtilsExtra
    to Cmake + Meson + PkgConf ...
  * Enable tests, add a whole bunch of required build-dependencies

 -- Alexandre Detiste <tchet@debian.org>  Thu, 13 Feb 2025 20:44:36 +0100

catfish (4.16.4-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 03:27:38 +0100

catfish (4.16.4-1) unstable; urgency=medium

  [ Sean Davis ]
  * New upstream release.
  * d/copyright: Bump copyright years
  * d/u/metadata: Bug-Browse -> Bug-Database

  [ Nilesh Patra ]
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Add d/salsa-ci.yml

 -- Sean Davis <sean@bluesabre.org>  Sun, 24 Jul 2022 20:20:19 +0530

catfish (4.16.3-1) unstable; urgency=medium

  [ Sean Davis ]
  * New upstream release.

  [ Diego M. Rodriguez ]
  * d/control: drop mlocate recommends (transitional) (Closes: #992740)

 -- Sean Davis <sean@bluesabre.org>  Sun, 26 Sep 2021 07:55:42 -0400

catfish (4.16.2-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Bump debhelper-compat to 13
    - Use new uploader email address
  * d/copyright:
    - Update upstream name, contact, and source

 -- Sean Davis <sean@bluesabre.org>  Wed, 04 Aug 2021 04:04:18 -0400

catfish (4.16.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.16.0
    - Drop upstream patch.
  * d/control:
    - Move python3-pexpect from Depends to Build-Depends.
    - Depend on gir1.2-xfconf-0.
    - Add plocate as primary alternative to the 'locate' group of Recommends.
    - Suggest gir1.2-zeitgeist-2.0.
  * d/gbp.conf: Enable pristine-tar.

 -- Unit 193 <unit193@debian.org>  Fri, 08 Jan 2021 20:55:54 -0500

catfish (1.4.13-2) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * Team upload
  * Add patch to replace tree.getiterator with tree.iter
    Fixes compatibility with Python 3.9 (Closes: #976189)
  * Use watch file standard version 4
  * Update upstream metadata
  * Update Standards Version to 4.5.1 (No changes required)

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 10 Dec 2020 17:48:31 +0100

catfish (1.4.13-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

  [ Sean Davis ]
  * New upstream release.
  * d/control:
    - Remove Jackson from Uploaders. Thanks!
  * d/upstream/metadata: Set correct repository

 -- Sean Davis <smd.seandavis@gmail.com>  Tue, 14 Jan 2020 21:39:26 -0500

catfish (1.4.10-1) unstable; urgency=medium

  * New upstream release.
    - Fixed default path exclusions. Closes: #926850
  * d/control: Bump debhelper version to 12

 -- Sean Davis <smd.seandavis@gmail.com>  Sat, 14 Sep 2019 04:59:53 -0400

catfish (1.4.8-2) unstable; urgency=high

  * Team upload.
  * Add missing dependency on python3-dbus. Thanks to broomdodger for the
    report in https://bugs.kali.org/view.php?id=5627

 -- Raphaël Hertzog <raphael@offensive-security.com>  Thu, 15 Aug 2019 16:15:44 +0200

catfish (1.4.8-1) unstable; urgency=medium

  [ Sean Davis ]
  * New upstream release.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

 -- Sean Davis <smd.seandavis@gmail.com>  Sun, 04 Aug 2019 08:27:17 -0400

catfish (1.4.7-1) unstable; urgency=medium

  * New upstream release.
    - Fixed context menu activation (LP: #1788058)
    - Fixed crash with searching inside /dev (LP: #1772437)
    - Fixed window button placement in LXDE (LP: #1512031)
    - Fixed occasional crash at startup (LP: #1753163)
  * d/control:
    - Bump Standards-Version to 4.3.0, no changes needed.
  * d/copyright:
    - Bump source copyright years to 2019

 -- Sean Davis <smd.seandavis@gmail.com>  Mon, 11 Feb 2019 18:44:12 -0500

catfish (1.4.6-1) unstable; urgency=medium

  [ Sean Davis ]
  * New upstream release.
    - Fixed PermissionError in settings handler (LP: #1567787)
    - Fixed UnicodeDecodeError in search (LP: #1646714)
    - Fixed detection of hidden files (LP: #1731608)
    - Fixed display of icons in Buuf themes. Closes: #816126
  * d/control:
    - Updated homepage to docs.xfce.org
  * d/upstream/signing-key.asc:
    - Dropped, Xfce release manager does not have key signing
  * d/watch:
    - Removed pgpsigurlmangle
    - Updated release pattern to archive.xfce.org

  [ Mattia Rizzolo ]
  * Bump Standards-Version to 4.2.1, no changes needed.

 -- Sean Davis <smd.seandavis@gmail.com>  Thu, 27 Sep 2018 17:15:37 -0400

catfish (1.4.5-1) unstable; urgency=medium

  [ SVN-Git Migration ]
  * Migrate the package from SVN to Git, update the Vcs-* fields accordingly.

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one.

  [ Sean Davis ]
  * New upstream release
  * d/control:
    - Update Standards-Version to 4.1.4
    - Add build dependency on dh-python. Closes: #896724
    - Run wrap-and-sort

  [ Mattia Rizzolo ]
  * d/control:
    + Bump Standards-Version to 4.1.5, no changes needed.
    + Set Rules-Requires-root:no.

 -- Sean Davis <smd.seandavis@gmail.com>  Mon, 09 Jul 2018 19:31:27 +0200

catfish (1.4.4-1) unstable; urgency=medium

  * New upstream release. Closes: #798074
    - Fixes en_AU localization. Closes: #794544
  * debian/compat:
    - Bump debhelper compatibility to 11
  * debian/control:
    - Update Standards-Version to 4.1.3
    - Updated VCS-Browser to secure origin
    - Add build dependency on python-gi-dev
  * debian/copyright:
    - Remove unused license
    - Update copyrights
    - Use HTTPS in the Format field
  * debian/menu:
    - Removed, resolves command-in-menu-file-and-desktop-file lintian
      warning
  * debian/rules:
    - Build with pybuild, non-existent tests are disabled to prevent
      failing builds on Python 3.5+
  * debian/patches:
    - Dropped, included upstream.
  * debian/watch:
    - Add upstream signing-key.asc

 -- Sean Davis <smd.seandavis@gmail.com>  Sun, 28 Jan 2018 06:45:49 -0500

catfish (1.2.2-1) unstable; urgency=medium

  * New upstream bugfix release.
    - Fixes opening previous selected item (LP: #1372165)
    - Fixes AttributeError on update dialog (LP: #1372166)

 -- Jackson Doak <noskcaj@ubuntu.com>  Mon, 22 Sep 2014 08:03:08 +1000

catfish (1.2.1-1) unstable; urgency=medium

  * New upstream release
    - Fix startup crash. Closes: #758652

 -- Sean Davis <smd.seandavis@gmail.com>  Tue, 19 Aug 2014 19:50:13 -0400

catfish (1.2.0-1) unstable; urgency=medium

  * New upstream release
    - Fix about dialogue version shown. Closes: #757828
  * debian/control: Make the PAPT the maintainer, Sean and myself uploaders

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 17 Aug 2014 08:28:04 +1000

catfish (1.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Refresh Manually-Install-Docs.patch

 -- Sean Davis <smd.seandavis@gmail.com>  Sat, 09 Aug 2014 00:02:10 -0400

catfish (1.0.2-2) unstable; urgency=medium

  * Team upload.
  * Change python version to python3, improves case-insensitive
    search for non-english file names (lp: #1280607)
  * debian/control:
    - Change dependencies to python3
    - Remove suggests for python-zeitgeist, no python3-zeitgeist
  * debian/rules:
    - Change build to python3
    - Removed obsolete line for pexpect license

 -- Sean Davis <smd.seandavis@gmail.com>  Sat, 29 Mar 2014 23:03:33 -0400

catfish (1.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Wed, 12 Mar 2014 07:11:06 +1100

catfish (1.0.1-2) unstable; urgency=medium

  * Add python-pexpect to depends

 -- Jackson Doak <noskcaj@ubuntu.com>  Fri, 07 Mar 2014 06:23:19 +1100

catfish (1.0.1-1) unstable; urgency=medium

  * New upstream release
    - Fix CVE-2014-2093, CVE-2014-2094, CVE-2014-2095, CVE-2014-2096.
      (Closes: #739958)

 -- Jackson Doak <noskcaj@ubuntu.com>  Fri, 28 Feb 2014 16:10:56 +1100

catfish (1.0.0-3) unstable; urgency=medium

  * debian/control: use python:depends instead of python3:depends.
    Closes: #739954, #738982

 -- Jackson Doak <noskcaj@ubuntu.com>  Wed, 26 Feb 2014 16:48:23 +1100

catfish (1.0.0-2) unstable; urgency=medium

  * Depend on python-gi-cairo. Closes: #738639

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 23 Feb 2014 07:40:04 +1100

catfish (1.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Jackson Doak ]
  * New upstream release. Closes: #589777, #598598 LP: #1257500, #1258713
  * Replace Depends: locate with Recommends: mlocate | locate. Closes: #731363
  * Bump standards-version to 3.9.5

  [ Vincent Cheng ]
  * Drop debian/catfish.1 and use upstream manpage instead.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 04 Feb 2014 21:03:50 -0800

catfish (0.8.2-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jackson Doak ]
  * Set myself as maintainer
  * New upstream release
  * Update manpage
  * Merge from ubuntu, changes:
    Closes: #641577, #664326, #579181, #524603, #579564, #589776, #713006
  * Create debian/catfish.manpages, debian/docs,
    debian/patches/Manually-Install-Docs.patch
  * Delete debian/README.source, we no longer use dpatch
  * debian/copyright:
    - Specify GPL-2+
    - Convert to debian format 1.0
  * debian/control:
    - Bump debhelper to 9
    - Use "canonical" VCS fields
    - Update description
  * debian/compat: Set as 9
  * Use new URL in debian/watch

  [ Sean Davis ]
  * debian/control
    - Updated standards version to 3.9.4
    - Updated homepage to launchpad
    - Remove dependencies on yelp, gksu
    - Move zeitgeist dependencies to Suggests
    - Update description to be more accurate
  * Update watch file for new home on Launchpad
    - update debian/watch

  [ Liviu Andronic ]
  * Misc changes to debian/control.
  * Simplify debian/rules to ensure it works with newest catfish.

  [ Scott Kitterman ]
  * Fix Makefile.in.in to install .py instead of .pyc files
  * Drop obsolete debian/patches/10Fix_makefile.dpatch and 00list
  * Drop dpatch from build-depends
  * Add --with python2 so dh_python2 is used

  [ Lionel Le Folgoc ]
  * debian/patches:
    - 20Fix_desktopfile.dpatch, 40Fix_gtkiconload.dpatch: dropped, applied
      upstream.
    - 30Fix_tracker_backend.dpatch: dropped, tracker support has been removed.
  * debian/control:
    - replace gtk2 b-deps with python-gi.
    - promote python-xdg to depends, the new mimetype filter seems to discard
      all results without it.
  * debian/source/format: switch to 3.0 (quilt).
  * Convert to dh_python2
    - update debian/rules
    - update debian/control

  [ Andrew Starr-Bochicchio ]
  * Fix file permissions during install.
  * Don't install extra license file.
  * Document missing copyright holder.

 -- Jackson Doak <noskcaj@ubuntu.com>  Mon, 14 Oct 2013 17:47:55 +1100

catfish (0.3.2-2) unstable; urgency=low

  * Team upload.

  [ Severin Heiniger ]
  * Fix lintian warning dh_desktop-is-deprecated in debian/rules.
  * Remove sclocate from Recommends as it does not appear to be used anymore.
    It only exists in old stable.
    + Closes: #532103.

  [ Jakub Wilk ]
  * Fix compatibility with tracker 0.8 (closes: #577205). Thanks to Michael
    Biebl for the bug report and the patch.
  * Add README.source.
  * Bump to Standards-Version 3.9.2 (no changes needed).
  * Add ${misc:Depends} to Depends.
  * Add build-arch and build-indep target. Make binary-arch target do nothing.
  * Prevent program crashing when an icon is missing from the icon theme
    (closes: #655430). Thanks to Carlos Kosloff for the bug report and John
     Church for the initial patch.
  * Remove reference to slocate from package description (see bug #532103).

 -- Jakub Wilk <jwilk@debian.org>  Thu, 09 Feb 2012 14:09:24 +0100

catfish (0.3.2-1) unstable; urgency=low

  [ Marco Rodrigues ]
  * New upstream version (0.3.1)
    + Closes LP: #316674
  * Bump to Standards-Version 3.8.0.

  [ Sandro Tosi ]
  * debian/watch
    - updated to reflect new upstream website
  * debian/control
    - Homepage field updated
  * debian/copyright
    - updated upstream location
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Savvas Radević ]
  * 10Fix_makefile.dpatch updated to follow upstream changes

  [ Piotr Ożarowski ]
  * New upstream version (0.3.2)
    + Closes: #511492
  * Updated the copyright years in debian/copyright
  * Updated catfish.svg link in debian/rules

 -- Python Applications Team <python-apps-team@lists.alioth.debian.org>  Tue, 13 Jan 2009 19:04:56 +0100

catfish (0.3-2) unstable; urgency=low

  * debian/patches/10Fix_makefile.dpatch: UPDATED
    - Disable the creation of symlinks in Makefile as we will
      handle it in debian/rules to ensure they are created correctly.
  * debian/rules:
    - Updated to create correct symlinks that were being incorrectly created in
      the Makefile.
  * Closes lp: #222050

 -- Cody A.W. Somerville <cody-somerville@ubuntu.com>  Thu, 22 May 2008 02:48:46 +0200

catfish (0.3-1) unstable; urgency=low

  * debian/control:
    - added python-gobject to Depends
    - removed dbus from Depends
    - moved python-xdg to Recommends
    - added slocate to Recommends
    - added "python-dbus, strigi-daemon, doodle, tracker, beagle" to Suggests
    - bumped Standards-Version to 3.7.3
    - added PAPT to Uploaders
    - added Vcs-Svn and Vcs-Browser fields
  * debian/rules:
    - Removed call to dh_shlibsdeps
  * debian/patches/20Fix_desktopfile.dpatch:
    - Remove encoding field from .desktop file

 -- Cody A.W. Somerville <cody-somerville@ubuntu.com>  Mon, 18 Feb 2008 16:52:28 -0800

catfish (0.3-0ubuntu2) hardy; urgency=low

  [Cody A.W. Somerville]
  * debian/watch: Created watchfile.
  * debian/rules:
    - Removed unrequired debhelper comments
    - Removed unneeded spaces at eol
    - Removed undeeded header
    - README now passed to dh_installdocs
  * debian/patches/10Disable_compile.dpatch:
    - Updated patch header
      . Added name before e-mail
      . Added patch description
  * debian/control:
    - Removed build-depends-indep python (not required)
    - Added homepage field (removed from long description)
    - Removed 'a' from start of package synopsis
    - Removed unneeded whitespace
    - Removed X[BS]-Python-Version fields
  * debian/docs: REMOVED (installed directly in debian/rules now).
  * debian/menu:
    - No longer uses absolute path
    - Updated to conform to new Debian menu policy

 -- Bryce Harrington <bryce@ubuntu.com>  Wed, 12 Dec 2007 18:37:33 -0800

catfish (0.3-0ubuntu1) hardy; urgency=low

  * New release 0.3
  * Added dbus dependency
  * Updated debian/docs
  * Updated makefile patch
  * Modified debian/rules to handle "clean" errors better

 -- Cody A.W. Somerville <cody-somerville@ubuntu.com>  Fri, 02 Nov 2007 17:24:10 -0300

catfish (0.1-0ubuntu1) feisty; urgency=low

  * Initial release

 -- Cody A.W. Somerville <cody-somerville@ubuntu.com>  Fri, 12 Jan 2007 22:54:55 -0400
